<?php

namespace App\Form;

use App\Entity\Data;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DataType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('numerSta')
            ->add('date')
            ->add('pmer')
            ->add('tend')
            ->add('codTend')
            ->add('dd')
            ->add('ff')
            ->add('t')
            ->add('td')
            ->add('u')
            ->add('vv')
            ->add('ww')
            ->add('w1')
            ->add('w2')
            ->add('n')
            ->add('nbas')
            ->add('hbas')
            ->add('cl')
            ->add('cm')
            ->add('ch')
            ->add('pres')
            ->add('nivBar')
            ->add('geop')
            ->add('tend24')
            ->add('tminsol')
            ->add('sw')
            ->add('tw')
            ->add('raf10')
            ->add('rafper')
            ->add('per')
            ->add('etatSol')
            ->add('htNeige')
            ->add('ssfrai')
            ->add('perssfrai')
            ->add('tn12')
            ->add('tn24')
            ->add('tx12')
            ->add('tx24')
            ->add('rr1')
            ->add('rr3')
            ->add('rr6')
            ->add('rr12')
            ->add('rr24')
            ->add('phenspe1')
            ->add('phenspe2')
            ->add('phenspe3')
            ->add('phenspe4')
            ->add('nnuage1')
            ->add('ctype1')
            ->add('hnuage1')
            ->add('nnuage2')
            ->add('ctype2')
            ->add('hnuage2')
            ->add('nnuage3')
            ->add('ctype3')
            ->add('hnuage3')
            ->add('nnuage4')
            ->add('ctype4')
            ->add('hnuage4')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Data::class,
        ]);
    }
}
