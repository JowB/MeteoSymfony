<?php

namespace App\Controller;

use App\Entity\Data;
use App\Form\DataType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/")
 */
class DataController extends Controller
{

    private $stations = array(
        "7005" => "ABBEVILLE",
        "7015" => "LILLE-LESQUIN",
        "7020" => "PTE DE LA HAGUE",
        "7027" => "CAEN-CARPIQUET",
        "7037" => "ROUEN-BOOS",
        "7072" => "REIMS-PRUNAY",
        "7110" => "BREST-GUIPAVAS",
        "7117" => "PLOUMANAC'H",
        "7130" => "RENNES-ST JACQUES",
        "7139" => "ALENCON",
        "7149" => "ORLY",
        "7168" => "TROYES-BARBEREY",
        "7181" => "NANCY-OCHEY",
        "7190" => "STRASBOURG-ENTZHEIM",
        "7207" => "BELLE ILE-LE TALUT",
        "7222" => "NANTES-BOUGUENAIS",
        "7240" => "TOURS",
        "7255" => "BOURGES",
        "7280" => "DIJON-LONGVIC",
        "7299" => "BALE-MULHOUSE",
        "7314" => "PTE DE CHASSIRON",
        "7335" => "POITIERS-BIARD",
        "7434" => "LIMOGES-BELLEGARDE",
        "7460" => "CLERMONT-FD",
        "7471" => "LE PUY-LOUDES",
        "7481" => "LYON-ST EXUPERY",
        "7510" => "BORDEAUX-MERIGNAC",
        "7535" => "GOURDON",
        "7558" => "MILLAU",
        "7577" => "MONTELIMAR",
        "7591" => "EMBRUN",
        "7607" => "MONT-DE-MARSAN",
        "7621" => "TARBES-OSSUN",
        "7627" => "ST GIRONS",
        "7630" => "TOULOUSE-BLAGNAC",
        "7643" => "MONTPELLIER",
        "7650" => "MARIGNANE",
        "7661" => "CAP CEPET",
        "7690" => "NICE",
        "7747" => "PERPIGNAN",
        "7761" => "AJACCIO",
        "7790" => "BASTIA",
        "61968" => "GLORIEUSES",
        "61970" => "JUAN DE NOVA",
        "61972" => "EUROPA",
        "61976" => "TROMELIN",
        "61980" => "GILLOT-AEROPORT",
        "61996" => "NOUVELLE AMSTERDAM",
        "61997" => "CROZET",
        "61998" => "KERGUELEN",
        "67005" => "PAMANDZI",
        "71805" => "ST-PIERRE",
        "78890" => "LA DESIRADE METEO",
        "78894" => "ST-BARTHELEMY METEO",
        "78897" => "LE RAIZET AERO",
        "78922" => "TRINITE-CARAVEL",
        "78925" => "LAMENTIN-AERO",
        "81401" => "SAINT LAURENT",
        "81405" => "CAYENNE-MATOURY",
        "81408" => "SAINT GEORGES",
        "81415" => "MARIPASOULA",
        "89642" => "DUMONT D'URVILLE"
    );

    /**
     * @Route("/", name="data_index", methods="GET")
     */
    public function index(): Response
    {
        $data = $this->getDoctrine()
            ->getRepository(Data::class)
            ->findAll();

        return $this->render('data/index.html.twig', ['data' => $data, 'stations' => $this->stations]);
    }

    /**
     * @Route("/new", name="data_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $data = new Data();
        $form = $this->createForm(DataType::class, $data);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($data);
            $em->flush();

            return $this->redirectToRoute('data_index');
        }

        return $this->render('data/new.html.twig', [
            'data' => $data,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="data_show", methods="GET")
     */
    public function show(Data $data): Response
    {
        return $this->render('data/show.html.twig', ['data' => $data]);
    }

    /**
     * @Route("/{id}/edit", name="data_edit", methods="GET|POST")
     */
    public function edit(Request $request, Data $data): Response
    {
        $form = $this->createForm(DataType::class, $data);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('data_edit', ['id' => $data->getId()]);
        }

        return $this->render('data/edit.html.twig', [
            'data' => $data,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="data_delete", methods="DELETE")
     */
    public function delete(Request $request, Data $data): Response
    {
        if ($this->isCsrfTokenValid('delete' . $data->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($data);
            $em->flush();
        }

        return $this->redirectToRoute('data_index');
    }
}
